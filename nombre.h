#ifndef NOMBRE_H_INCLUDED
#define NOMBRE_H_INCLUDED











struct listech
{
  int donne;
  struct listech *suivant;
};

void creerList(struct listech** tete);
void afficherList(struct listech* tete);
int listLongeur(struct listech* tete);
void addlistech(struct listech** tete, int donne, int position);
int searchList(struct listech* tete, int donne);
void sortList(struct listech** tete);
void addSortedNode(struct listech** tete, int donne);
void removeDuplicates(struct listech* tete);
void menu(void);


#endif
